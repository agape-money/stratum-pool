const bitcoin = require('bitgo-utxo-lib')
const util = require('./util.js')

const scriptCompile = addrHash => bitcoin.script.compile([
    bitcoin.opcodes.OP_DUP,
    bitcoin.opcodes.OP_HASH160,
    addrHash,
    bitcoin.opcodes.OP_EQUALVERIFY,
    bitcoin.opcodes.OP_CHECKSIG
])

const scriptFoundersCompile = address => bitcoin.script.compile([
    bitcoin.opcodes.OP_HASH160,
    address,
    bitcoin.opcodes.OP_EQUAL
])

// public members
let txHash
exports.txHash = () => txHash

exports.createGeneration = (rpcData, blockReward, feeReward, recipients, poolAddress, poolHex, coin) => {
    let poolAddrHash = bitcoin.address.fromBase58Check(poolAddress).hash; 

    let network = coin.network
    //console.log('network: ', network)
    let txb = new bitcoin.TransactionBuilder(network)

    // Set sapling or overwinter to either true OR block height to activate.
    // NOTE: if both are set, sapling will be used.
    if (coin.sapling === true || (typeof coin.sapling === 'number' && coin.sapling <= rpcData.height)) {
        txb.setVersion(bitcoin.Transaction.ZCASH_SAPLING_VERSION);
    } else if (coin.overwinter === true || (typeof coin.overwinter === 'number' && coin.overwinter <= rpcData.height)) {
        txb.setVersion(bitcoin.Transaction.ZCASH_OVERWINTER_VERSION);
    }

    // input for coinbase tx
    let blockHeightSerial = (rpcData.height.toString(16).length % 2 === 0 ? '' : '0') + rpcData.height.toString(16)

    let height = Math.ceil((rpcData.height << 1).toString(2).length / 8)
    let lengthDiff = blockHeightSerial.length / 2 - height
    for (let i = 0; i < lengthDiff; i++) {
        blockHeightSerial = `${blockHeightSerial}00`
    }

    let length = `0${height}`
    let serializedBlockHeight = new Buffer.concat([
        new Buffer(length, 'hex'),
        util.reverseBuffer(new Buffer(blockHeightSerial, 'hex')),
        new Buffer('00', 'hex') // OP_0
    ])

    txb.addInput(new Buffer('0000000000000000000000000000000000000000000000000000000000000000', 'hex'),
        4294967295,
        4294967295,
        new Buffer.concat([
            serializedBlockHeight,
            // Default s-nomp pool https://github.com/s-nomp/s-nomp/wiki/Insight-pool-link
            Buffer(poolHex ? poolHex : '44656661756C7420732D6E6F6D7020706F6F6C2068747470733A2F2F6769746875622E636F6D2F732D6E6F6D702F732D6E6F6D702F77696B692F496E73696768742D706F6F6C2D6C696E6B', 'hex')
        ])
    )

    // calculate total fees
    let feePercent = 0
    recipients.forEach(recipient => feePercent += recipient.percent)

    // Zcash uses fundingstreams from getblocksubsidy and no longer uses founders addresses in coins
    if(coin.vFundingStreams) {

	// this is greatly changed from the original - I honestly can't tell
	// what the original calculation was intended to accomplish
	// it seems like a mistake: 
	// Math.round(Math.round(blockReward.total * 
	// 	(1 - (coin.percentFoundersReward + feePercent) / 100) + feeReward))
	// that /100 seems like it will always result in a small fraction
	// of the total award being allocated - I don't get it
	 
        // pool t-addr
        txb.addOutput(
            scriptCompile(poolAddrHash),
            Math.round(blockReward.miner + feeReward)
        );

        // loop through founders and add them to our coinbase transaction
        rpcData.fundingstreams.map((founderItem) => {
            let foundersAddrHash = bitcoin.address.fromBase58Check(founderItem.address).hash;

            txb.addOutput(
                scriptCompile(foundersAddrHash),
                founderItem.valueZat
            );
        });
    }

    // Segwit support
    if (rpcData.default_witness_commitment !== undefined) {
        txb.addOutput(new Buffer(rpcData.default_witness_commitment, 'hex'), 0);
    }

    // pool fee recipients t-addr
    if (recipients.length > 0 && recipients[0].address != '') {
        let burn = 0
        if (coin.burnFees) {
            burn = feeReward
        }
        recipients.forEach(recipient => {
            txb.addOutput(
                scriptCompile(bitcoin.address.fromBase58Check(recipient.address).hash),
                Math.round(blockReward.total * (recipient.percent / 100) - burn)
            )
            burn = 0
        })
    }

    let tx = txb.build()

    txHex = tx.toHex()
    // console.log('hex coinbase transaction: ' + txHex)

    // assign
    txHash = tx.getHash().toString('hex')

    // console.log(`txHex: ${txHex.toString('hex')}`)
    // console.log(`txHash: ${txHash}`)

    return txHex
}

module.exports.getFees = feeArray => {
    let fee = Number()
    feeArray.forEach(value => fee += Number(value.fee))
    return fee
}
